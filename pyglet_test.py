import pyglet
import pywavefront
from pywavefront import visualization




window = pyglet.window.Window()
obj = pywavefront.Wavefront('Housing.obj')
label = pyglet.text.Label('Hello, world',
                          font_name='Times New Roman',
                          font_size=36,
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')

@window.event
def on_draw():
    window.clear()
    
    #label.draw()
    visualization.draw(obj)
    pyglet.graphics.draw(2, pyglet.gl.GL_POINTS,
    ('v2i', (10, 15, 30, 35))
)

pyglet.app.run()