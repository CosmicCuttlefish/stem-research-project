import numpy as np
from OpenGL.GL import *
import glfw
import pywavefront
from pywavefront import visualization

glfw.init()
window = glfw.create_window(900, 700, "PyOpenGL Test", None, None)
glfw.make_context_current(window)

vertices = [-0.5, -0.5,0.0,
             0.5, -0.5,0.0,
             0.0, 0.5,0.0]

v = np.array(vertices, dtype = np.float32)

obj = pywavefront.Wavefront('Housing.obj')


visualization.draw(obj)

# this will set a color for your background
glClearColor(0, 0, 0, 0)

while not glfw.window_should_close(window):
    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT)
    glDrawArrays(GL_TRIANGLES,0,3)
    glfw.swap_buffers(window)

glfw.terminate()





