from madcad.generation import subdivide
import numpy as np
import trimesh
from trimesh.voxel import creation
import sys
np.set_printoptions(threshold=sys.maxsize)

mesh = trimesh.load_mesh('Housing.obj')

mesh2 = trimesh.voxel.creation.voxelize(mesh, 1, method='subdivide')

test1 = mesh2.matrix.shape


for h in range(test1[0]):
    for i in range(test1[1]):
        for j in range(test1[2]):
            print(mesh2.matrix[h,i,j])
            print(j)

print(test1[2])