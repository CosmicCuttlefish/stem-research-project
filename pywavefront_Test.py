import pywavefront
from pywavefront import visualization

#[create a window and set up your OpenGl context]
obj = pywavefront.Wavefront('Housing.obj')

#[inside your drawing loop]
visualization.draw(obj)