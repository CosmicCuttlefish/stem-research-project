import obj2stl
from madcad.hashing import PositionMap
from madcad.io import read
from madcad import *
import numpy as np

obj2stl.convert(input='Housing.obj', output='Housing.stl')