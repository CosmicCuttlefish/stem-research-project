from madcad.hashing import PositionMap
from madcad.io import read
from madcad import *
import numpy as np


# load the obj file in a madcad Mesh
mymesh = read('Housing.obj')
# choose the cell size of the voxel
size = 1

voxel = list()    # this is a sparse voxel
hasher = PositionMap(size)   # ugly object creation, just to use one of its methods
for face in mymesh.faces:
    voxel.append(hasher.keysfor(mymesh.facepoints(face)))

print(voxel[0])
#for i in voxel:

show(mymesh)
