# README:
Final paper is stored in ./Notebook/Final-Paper/build/FinalDraft.pdf

A plain text version of the final paper is stored at ./Notebook/Final-Paper/FinalDraft.md

Notebook files are stored in ./Notebook

Bibtex citations are stored in ./Notebook/Final-Paper/Citations.bib as well as the end of the final Paper

The Abstract is stored in ./Notebook/Final-Paper/Abstract.md
