from madcad.generation import subdivide;
import numpy as np;
import trimesh;
from trimesh.voxel import creation;
import sys;
import time;
import logging;

# Setting Up Logging
trimesh.util.attach_to_log()
localtime = time.asctime( time.localtime(time.time()) )
print ("Local current time :", localtime)
logging.basicConfig(level=logging.DEBUG)

file_path = input("Enter file path, please use the entier path")
mesh = trimesh.load_mesh(file_path)

#while mesh.is_watertight != False: file_path = input("Mesh is not watertight, Please try again.")

resolution = float(input("Please input  voxel resolution in mm."))

while type(resolution) != float:
 resolution = float(input("Please try again."))

print('press "q" if the visualization is correct')
mesh.show()


mesh = trimesh.voxel.creation.voxelize(mesh, resolution, method='subdivide')

mesh.show()
