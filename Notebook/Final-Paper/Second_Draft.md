---

title: "The Document Title"
author: [Example Author, Another Author]
date: "2017-02-20"
keywords: [Markdown, Example]
...

### 1: Introduction

#### 1.1 Motivation

3D printing has changed the way engineers and makers interact to design and fabricate parts. A core part of the development and growth of 3D printing has been driven by open source projects, lowering the costs and barriers to entry for these machines(ASME). However, as 3D printing has become more mainstream the open-source roots of many of these projects has been left behind in favor of profitability. This is especially apparent in Stereolithography Apparatus (SLA) style printers where one company has locked down the primary model preparation program (slicer). There are multiple open source projects dedicated to providing a high quality slicing software but these are primarily directed towards Fused Deposition Modeling (FDM) printing or specific printers. In an attempt to fill a gap and to grow my own programming skills I will write a command line slicer oriented towards SLA printing.

#### 1.2 Evaluation metrics

To measure the relative success of this program the following metrics will be used:

- The time it takes for the program to process the files;

- The maximum supported resolution;

- The ease of use, ie how quickly can someone versed in traditional slicers start working with the program;

- The feature set of the program;

- The relative quality of output files;

While it is unlikely for my program to outperform other slicers these metrics should show areas for improvement.

#### 1.3 Background Information:

Stereolithographic Apparatus (SLA printers) use a UV Liquid Crystal Display(LCD) or Digital Light Processing(DLP) chip to project UV light into a vat of UV curable resin. This cures the resin in precise shapes before lifting the layer up a few fractions of a millimeter and repeating the process.

Slicers: Slicers take 3D models and convert them into commands for the D printer to follow. For SLA printers this involves breaking the model down into 'voxels' or 3D pixels, these voxels tell the printer what pixels in the LCD or DLP chip to have on and off for each layer. Slicers are often used for creating support materials and, for FDM printing, setting infill density. From this point forward slicer will only be referring to SLA slicers, not FDM slicers.

Voxels are essentially 3D pixels, cubes of a set size that either store color values, or as a plain boolean value if color is not important.

### 2: Design process and breakdown

#### 2.1 Language choice

Python was used as the primary programming language due to it's interpreted nature making rapid iteration easy and efficient. The human-readability making the code easy to debug and explain. And the massive catalog of preexisting libraries written in fast languages that reduce system workload. And the high level APIs allow for relatively few lines of code to provide powerful functionality.

#### 2.2 High level overview

Details on the slicer, how it works, libraries used, etc.

The easiest way to slice models for SLA printers is to use boolean voxels stored in a 3D array with a size value denoted. To handle voexlization and file processing the trimesh library was used, supported by the pymadcad library. After importing and processing the object file using trimesh the data can be manipulated using numpy (numerical python). Outputs can be formatted as either plain binary, XML files, plain text, or eventually be piped into ctb files. 

#### 2.3 Code review

This is the testing code used to test voxelization and precision

```python
from cgi import test
from madcad.generation import subdivide
import numpy as np
import trimesh
from trimesh.voxel import creation
import sys
np.set_printoptions(threshold=sys.maxsize)

resolution = 1
#mesh =  trimesh.load_mesh('CuteOcto.stl')

mesh =  trimesh.load_mesh('Housing.obj')

print(mesh.is_watertight)
mesh.show()
#mesh.apply_transform(trimesh.transformations.random_rotation_matrix())

mesh2 = trimesh.voxel.creation.voxelize(mesh, resolution, method='subdivide')




# Debugging tools #

print(mesh.euler_number)

print(type(mesh2))

print(mesh2.matrix[0,0])

#mesh.show()
mesh2.show()
```

Breaking down the code, this imports the necessary libraries for logging, debugging and the core functionality of the program.

```python
from madcad.generation import subdivide;
import numpy as np;
import trimesh;
from trimesh.voxel import creation;
import sys;
import time;
import logging;
```

```python
#mesh =  trimesh.load_mesh('CuteOcto.stl')

mesh =  trimesh.load_mesh('Housing.obj')
```

These lines import the test files, both .obj and .stl files can be read by the same code, however others require more work.

```python
print(mesh.is_watertight)
```

This code prints the status of the mesh to the terminal, if the mesh is not watertight it can cause errors during the voxelization process

```python
mesh.show()
```

Mesh.show is used to visualize the mesh and any changes made

```python
mesh.apply_transform(trimesh.transformations.random_rotation_matrix())
```

This line is used to apply random transformations to the object (rotation, translation, scaling). used to test the algorithms ability to handle complex shapes.

```python
mesh2 = trimesh.voxel.creation.voxelize(mesh, resolution , method='subdivide')
```

This line runs the voxelization algorithm, ```mesh``` refers to the object getting voxelized, ``resolution`` is measured in millimeters, ``method='subdivide'`` is the voxelization method, 

```python
print(mesh.euler_number)
```

Prints the Euler characteristic (a topological invariant) for the mesh In order to guarantee correctness, this is used like all upcoming lines for debugging.

```python
print(type(mesh2))
```

Prints the data type of the matrix to the terminal, used when learning the library.

```python
print(mesh2.matrix[0,0])
```

Returns matrix values for testing.

### 2.4 Final Code

The final code includes extra code for improving human interaction and removes many of the debugging features.

### 3: Evaluation

#### 3.1 Evaluation procedure

In testing the slicer I used several pre-designed models with known attributes that I could examine. The first file was a simple 20mm cube, this was to confirm I was getting reliable outputs and that the code was working. The second file was a gearbox component from a previous project, this allowed me to test complex geometry as the file had overhangs, complex curves, and precise size requirements. The final model was a model 'cute baby octopus says hello' sourced from thingiverse, This model consisted entirely of complex curves that would act as a stress test on the program. Comparison of models would be done by hand, looking for failures in the slicing process digitally. Speed tests would start when the slice button was pressed. For UX input two other makers in my community would provide feedback after using the project.

#### 3.2 Testing apparatus

Tests were conducted on the following hardware and operating systems:

|                | System 1                       | System 2                       |
| -------------- | ------------------------------ | ------------------------------ |
| OS             | Ubuntu 20.04 LTS               | Manjaro Linux x86_64           |
| Kernel Version | 5.11.0-46-generic              | 5.13.19-2-MANJARO              |
| Python Version | Python 3.8.10                  | Python 3.10.1                  |
| CPU            | Intel i9-9900K (16) @ 5.000GHz | Intel i5-10210U (8) @ 4.200GHz |
| Memory         | 32GB @ 3200MHz                 | 16GB @ 2666MHz                 |
| Notes          | Custom Built System            | System76 Lemur Pro rev9        |

#### 3. Imperial Evaluation

| Test                         | ChituBox | Project Code |
| ---------------------------- | -------- | ------------ |
| Slice Time                   |          |              |
| Maximum supported resolution |          | 0.2 mm       |
| Program start time           |          |              |

#### 3. Peer evaluation

Evaluation:

Results:

Elephant in the room:

Now the elephant in the room: in May ChiTu Systems, the makers of Chitubox, pushed an update encrypting the main-boards of LCD printers using their hardware, locking users into their proprietary ctb file format. Because of this update my slicer will not work with most modern SLA printers, there is a SDK that I have applied for but as of the time of writing I do not have access to the SDK.

Future Plans
In future I plan on implementing the chitubox SDK to allow my slicer to work with modern encrypted ChiTu systems printers.
