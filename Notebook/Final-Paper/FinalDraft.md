---
title: "Open-source 3D Model Slicing for Stereolithography Apparatus Printing"
date: \today

author: "Jan Eshun"
bibliography: "Citations.bib"
link-citations: true
urlcolor: "blue"
geometry: margin=1in
header-includes:
  - \usepackage{setspace}
  - \doublespacing
  - \usepackage{fancyhdr}
  - \pagestyle{fancy}
  - \fancyhead[R]{\thepage}
  - \fancyhead[L]{ }
  - \fancyfoot[C]{ }

  - \usepackage{sectsty}
  - \allsectionsfont{\normalsize}
  - \sectionfont{\centering \normalsize}


  - \usepackage{caption}
  - \captionsetup[figure]{labelformat=empty}

include-before:
- '`\newpage{}`{=latex}'

mainfont: Source Serif 4
monofont: SauceCodePro Nerd Font

cite-method: citeproc
csl: ./citationstyle.csl

indent: true

nocite: |
  @havil2019
  @DawsonHaggerty2020
  @Holmer
  @Lau
  @Math2011
  @NumPy
  @Research2021
  @2021
  @Staff2015a
  @Staff2015
  @Systems2021
  @Terzopoulos1994
  @Dassault-Systems
---


\renewcommand{\headrulewidth}{0pt}

\newpage{}

# 1: Introduction

## 1.1 Motivation

3D printing has changed the way engineers and makers interact with, design, and fabricate parts. A core part of the development and growth of 3D printing has been driven by open-source projects, lowering the costs and barriers to entry for 3D printing [@Gonzalez2020]. However, as 3D printing has become more mainstream, the open-source roots of many of these projects have been left behind in favor of profitability. This is especially apparent in Stereolithography Apparatus (SLA) style printers, where one company has locked down the primary model preparation program (slicer). There are multiple open-source projects dedicated to providing high-quality slicing software, but these are primarily directed towards Fused Deposition Modeling (FDM) printing, or specific printers. This project will build the basis of an open-source SLA slicer.

## 1.2 Evaluation Metrics

To measure the relative success of this program, the following metrics will be used:

- The time it takes for the program to process the files;

- The minimum supported resolution;

- The ease of use, i.e. how quickly someone versed in traditional slicers can start working with the program;

- The relative quality of output files.

While it is unlikely for the program to outperform the commercial alternative, these metrics should show areas for improvement.

## 1.3 Background Information:

Stereolithographic Apparatus (SLA) printers use either a UV Liquid Crystal Display (LCD), or a Digital Light Processing (DLP) chip, to project UV light into a vat of UV-curable resin. The idea is to cure the resin in precise patterns, one layer at at time. After each pass, the layer is lifted a few fractions of a millimeter, and the process is repeated until the full model is printed.

A 3D model is commonly represented as a mesh, a series of external triangular faces that define the shape of the object. An example of a simple mesh can be seen below(Figure 1): 

![](Low-Poly-Octo.png)

Figure 1: An Example of the CuteOcto file. A lower polygon count is used here to help visualize the concept.  

While mesh-based file formats are commonly used in the distribution of 3D models, they need to be converted before being printed. Slicers take 3D models in the form of meshes and convert them into commands for the 3D printer to follow. For SLA printers this involves breaking the model down into 'voxels'. Voxels are essentially 3D pixels, cubes of a set size that either store color values, or a plain boolean value if color is not important. These voxels tell the printer what pixels in the LCD or DLP chip to turn on and off for each layer. Below is an example of a 3D model, CuteOcto, before and after voxelization (Figures 2 & 3). 

![](CuteOcto.svg)
Figure 2: An Example of the CuteOcto file an an STL file.



![](CuteVoto.svg)
Figure 3: An Example of the CuteOcto file voxelized. The voxels are large here to illustrate the concept.  

In addition to describing the exterior shape of the object, slicers also describe support materials and infill at a prescribed density and pattern. The output of the slicer is a 'gcode' file. Gcode is the primary method of communicating with 3D printers. Some printers rely on alternative formats not covered here. This project will only deal with SLA slicing.

# 2: Design Process and Breakdown

## 2.1 Language Choice

Python was used as the primary programming language due to its interpreted nature, making rapid iteration easy and efficient. It's human-readability makes the code easy to debug and explain. It boasts a massive catalog of preexisting libraries written in fast languages that reduce system workload. And the high-level APIs allow for relatively few lines of code to provide powerful functionality.

## 2.2 High Level Overview


The easiest way to slice models for SLA printers is to use boolean voxels stored in a 3D array with a size value denoted. To handle voxelization and file processing the trimesh library was used, supported by the pymadcad library. After importing and processing the object file using trimesh, the data can be manipulated using NumPy (numerical python). Outputs can be formatted as either plain binary, XML files, plain text, or eventually, be piped into ctb files.


## 2.3 Code Review

This is the testing code used to test voxelization and precision:

```python
from cgi import test
from madcad.generation import subdivide
import numpy as np
import trimesh
from trimesh.voxel import creation
import sys
np.set_printoptions(threshold=sys.maxsize)

resolution = 1
#mesh =  trimesh.load_mesh('CuteOcto.stl')

mesh =  trimesh.load_mesh('Housing.obj')

print(mesh.is_watertight)
mesh.show()
#mesh.apply_transform(trimesh.transformations.random_rotation_matrix())

mesh2 = trimesh.voxel.creation.voxelize(mesh, resolution, method='subdivide')




# Debugging tools #

print(mesh.euler_number)

print(type(mesh2))

print(mesh2.matrix[0,0])

#mesh.show()
mesh2.show()
```

Breaking down the code, this imports the necessary libraries for logging, debugging, and the core functionality of the program.

```python
from madcad.generation import subdivide;
import numpy as np;
import trimesh;
from trimesh.voxel import creation;
import sys;
import time;
import logging;
```

```python
#mesh =  trimesh.load_mesh('CuteOcto.stl')

mesh =  trimesh.load_mesh('Housing.obj')
```

These lines import the test files, both .obj and .stl files can be read by the same code, however others require more work.

```python
print(mesh.is_watertight)
```

This code prints the status of the mesh to the terminal, if the mesh is not watertight it can cause errors during the voxelization process.

```python
mesh.show()
```

Mesh.show is used to visualize the mesh and any changes made.

```python
mesh.apply_transform(trimesh.transformations.random_rotation_matrix())
```

This line is used to apply random transformations to the object (rotation, translation, scaling). Used to test the algorithm's ability to handle complex shapes.

```python
mesh2 = trimesh.voxel.creation.voxelize(mesh, resolution , method='subdivide')
```

This line runs the voxelization algorithm, ```mesh``` refers to the object getting voxelized, ```` resolution ```` is measured in millimeters, ``method='subdivide'`` is the voxelization method.

```python
print(mesh.euler_number)
```

Prints the Euler characteristic (a topological invariant) for the mesh in order to guarantee correctness, this is used like all upcoming lines for debugging.

```python
print(type(mesh2))
```

Prints the data type of the matrix to the terminal, used when learning the library.

```python
print(mesh2.matrix[0,0])
```

Returns matrix values for testing.

## 2.4 Final Code

The final code includes extra code for improving human interaction and removes many of the debugging features.

# 3: Evaluation

## 3.1 Evaluation Procedure

In testing the slicer several pre-designed models were used with known attributes that could be examined. The first file was a simple 20mm cube. This was used to confirm that reliable outputs were being generated and that the code was working. The second file was a gearbox component from a previous project. This allowed for testing complex geometry with overhangs, complex curves, and precise size requirements. The final model was 'cute baby octopus says hello' sourced from Thingiverse[@Jason2012]. This model consisted entirely of complex curves that would act as a stress test on the program. Comparison of models was done by hand, looking for failures in the slicing process digitally. Speed tests started when the slice button was pressed. The closed source point of comparison was the ChiTuBox software by ChiTu Systems.

## 3.2 Testing Apparatus

Tests were conducted on the following hardware and operating systems:

|                | System 1                       | System 2                       |
| -------------- | ------------------------------ | ------------------------------ |
| OS             | Ubuntu 20.04 LTS               | Manjaro Linux x86_64           |
| Kernel Version | 5.11.0-46-generic              | 5.13.19-2-MANJARO              |
| Python Version | Python 3.8.10                  | Python 3.10.1                  |
| CPU            | Intel i9-9900K (16) @ 5.000GHz | Intel i5-10210U (8) @ 4.200GHz |
| Memory         | 32GB @ 3200MHz                 | 16GB @ 2666MHz                 |
| Notes          | Custom Built System            | System76 Lemur Pro rev9        |

## 3.3 Empirical Evaluation

| Test                         | ChiTuBox | Project Code             |
| ---------------------------- | -------- | ------------------------ |
| Slice Time                   |>1 second |34 seconds before timeout |
| Minimum supported resolution |   N/A    | 0.2 mm                   |

## 3.4 Conclusion


ChiTuBox is unequivocally the better piece of software: it supports a much wider range of printers; it can process files much faster; and it generates instructions for support material. Nonetheless, this project demonstrates a proof of concept for the ideas and methods for designing open-source SLA slicers, and there is a clear path for developing a competitive alternative to ChiTuBox.


## 3.5 Next Steps

Python, while an excellent language for rapid prototyping, lacks the performance other languages offer, severely limiting the scalability of the slicer as implemented. The next step would be to rewrite the program in a language like C++ or Rust to decrease slice times and improve scalability. In order to support ctb enabled printers the ChiTuBox SDK would be incorporated. Finally, the slicing method would be generalized to support printers with different Z-step and display resolutions.

\pagebreak


# Reference
