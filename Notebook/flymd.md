# Introduction
3D printing has changed the way engineers and makers interact to design and fabricate parts. A core part of the development and growth of 3d printing has been driven by open source projects, lowering the costs and barriers to entry for these machines(ASME). However, as 3D printing has become more mainstream the open-source roots of many of these projects has been left behind in favor of profitability. This is especially apparent in Stereolithography Apparatus (SLA) style printers where one company has locked down the primary model preparation program (slicer). There are multiple open source projects dedicated to providing a high quality slicing software but these are primarily directed towards Fused Deposition Modeling (FDM) printing or specific printers. In an attempt to fill a gap and to grow my own programming skills I will write a command line slicer oriented towards SLA printing.

To measure the relative success of this program the following metrics will be used:
The time it takes for the program to process the files;
The maximum supported resolution;
The time it takes for the program to start;
The ease of use, ie how quickly can someone versed in traditional slicers start working with the program;
The feature set of the program;
The relative quality of output files;
While it is unlikely for my program to outperform other slicers these metrics should show areas for improvement.

Background Information:

Stereolithographic Apparatus (SLA printers) use a UV Liquid Crystal Display(LCD) or Digital Light Processing(DLP) chip to project UV light into a vat of UV curable resin. This cures the resin in precise shapes before lifting the layer up a few fractions of a millimeter and repeating the process.

Slicers: Slicers take 3d models and convert them into commands for the 3d printer to follow. For SLA printers this involves breaking the model down into 'voxels' or 3d pixels, these voxels tell the printer what pixels in the LCD or DLP chip to have on and off for each layer. Slicers are often used for creating support materials and, for FDM printing, setting infill density. From this point forward slicer will only be referring to SLA slicers, not FDM slicers.


Designing the Slicer:

Python was chosen for a few reasons:
 Python is easy to learn and easy to debug
 Python is an interpreted language that encourages rapid itteration
 Python has a massive catalog of libraries that reduce my workload.

Details on the slicer, how it works, libraries used, etc.

Evaluation:
In testing the slicer I used several pre-designed models with known attributes that I could examine. The first file was a simple 20mm cube, this was to confirm I was getting reliable outputs and that the code was working. The second file was a gearbox component from a previous project, this allowed me to test complex geometry as the file had overhangs, complex curves, and precise size requirements. The final model was a model 'cute baby octopus says hello' sourced from thingiverse, This model consisted entirely of complex curves that would act as a stress test on the program. Comparison of models would be done by hand, looking for failures in the slicing process digitally. Speed tests would start when the slice button was pressed. For UX input two other makers in my community would provide feedback after using the project.fLyMd-mAkEr

Results:

Elephant in the room:

Now the elephant in the room: in May ChiTu Systems, the makers of Chitubox, pushed an update encrypting the mainboards of LCD printers using their hardware, locking users into their proprietary ctb file format. Because of this update my slicer will not work with most modern SLA printers, there is a SDK that I have applied for but as of the time of writing I do not have access to the SDK.

Future Plans
In future I plan on implementing the chitubox SDK to allow my slicer to work with modern encrypted ChiTu systems printers.
