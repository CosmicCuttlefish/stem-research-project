import pyglet
import ratcave as rc
from pyglet.window import key

# Create Window
window = pyglet.window.Window(1280,1280, resizable=True)
keys = key.KeyStateHandler()
window.push_handlers(keys)

def update(dt):
    pass
pyglet.clock.schedule(update)

# Insert filename into WavefrontReader.
obj_filename = rc.resources.obj_primitives
obj_reader = rc.WavefrontReader('Housing.obj')

# Create Mesh
monkey = obj_reader.get_mesh('Housing.obj', scale=.1)
monkey.position.xyz = 0, 0, -10.0
monkey.rotation.x = 0

# Create Scene
scene = rc.Scene(meshes=[monkey])
#scene.bgColor = 1, 0, 0

def rotate_meshes(dt):
    monkey.rotation.y += 30 * dt  
pyglet.clock.schedule(rotate_meshes)

def move_camera(dt):
    camera_speed = 3
    if keys[key.LEFT]:
        scene.camera.position.x -= camera_speed * dt
    if keys[key.RIGHT]:
        scene.camera.position.x += camera_speed * dt
    if keys[key.UP]:
        scene.camera.position.y += camera_speed * dt
    if keys[key.DOWN]:
        scene.camera.position.y -= camera_speed * dt
    if keys[key.MOD_SHIFT]:
        scene.camera.position.z += camera_speed * dt
    if keys[key.MOD_CTRL]:
        scene.camera.position.z -= camera_speed * dt
pyglet.clock.schedule(move_camera)

@window.event
def on_draw():
    with rc.default_shader:
        scene.draw()

pyglet.app.run()