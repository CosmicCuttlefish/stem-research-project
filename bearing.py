'''
	This is an example to generate a bearing from scratch. Of course it's even better to use the provided function in module `madcad.standard`
'''

from madcad import *

interfaces = [
        Circle((vec3(0,0,3),vec3(0,0,1)), 1),
        Circle((vec3(-1,-1,-1),normalize(vec3(-1,-1,-1))), 1),
        Circle((vec3(1,-1,-1),normalize(vec3(1,-1,-1))), 1),
        Circle((vec3(0,1,0),normalize(vec3(0,1,-1))), 1),
        ]

m = junction(
                interface[0],
                interface[1],
                interface[2],
                (interface[3], 'normal'),
                tangents='tangent',
                )
for c in interface:
        m += extrusion(c.axis[1]*3, web(c))